# task1.py

import multiprocessing as mp
from  lib.data_shaping import data_shaping
from  lib.reduce_item import reduce_func
from  lib.splitting import splitting_csv
from  lib.map import map_func
from  lib.shuffle import shuffle
from  lib.task1_write2csv import task1_wirte2csv
from  lib.task1_check_value import   task1_check
from  lib.data_shaping import  data_shaping




if __name__ == '__main__':
    with mp.Pool(processes=mp.cpu_count()) as pool:

        #   Number of Processes
        processes_num = mp.cpu_count()
        print("your CPU has \033[31m%d\033[0m cores" %(processes_num))


        #   Read the columns from the file for which you want data.
        map_in = splitting_csv(r"input_data\AComp_Passenger_data.csv", 2, processes_num)
        #print(map_in)
        #print(type(map_in))

        #   Check format.
        map_in_after_check = pool.map(task1_check, map_in, chunksize=int(len(map_in)/processes_num))
        #print(map_in_after_check)

        #   Shape data.
        map_in_after_check = data_shaping(map_in_after_check)
        #print(map_in_after_check)

        #   Map phase.
        map_out = pool.map(map_func, map_in_after_check, chunksize=int(len(map_in)/processes_num))
        #print(map_out)

        #   shuffle phase.
        reduce_in = shuffle(map_out)
        #print(reduce_in)
        
        #   reduce_out is a list.
        reduce_out = pool.map(reduce_func, reduce_in.items(), chunksize=int(len(reduce_in.keys())/processes_num))
        #print(reduce_out)

        #   use sorted() to sort the list and use the second values as key, descending order.
        output_high_to_low = sorted(reduce_out, key=lambda k:  k[1], reverse=True)
        #print(output_high_to_low)


        task1_wirte2csv(output_high_to_low)
        print('The output csv file of task1 is in the \\output_data\\task1_output_check.csv", the csv name is %c\033[31mtask1_output_check.csv\033[0m%c' % (34,34))
        





        



