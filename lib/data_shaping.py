# data_shaping.py


def data_shaping(data_in):
    """ Used for data shaping.

    Converts a two-dimensional list into a thought list so that the data can be processed by later functions.

    Args:
        data_in:   Two-dimensional list.

    Returns:
        code_list: A one-dimensional list containing all the input data.
    """

    #   Used for store the value from input list.
    code_list = []

    #   iterate over a list and store the value into "code_list"
    for i in data_in:
        for n in i:
            code_list.append(n)

    #print(code_list)
    return code_list


if __name__ == '__main__':
    test = [['DEN', 'JFK', 'ORD', 'ORD'],['LAS', 'PEK', 'BKK', 'IAH', 'CGK'], ['FCO', 'MUC', 'KUL', 'PVG']]
    print(data_shaping(test))