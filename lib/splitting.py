#   splitting.py
import csv
import numpy as np


def splitting_csv(csv_file, column_number, split_num):
    """Retrieve the specified columns of a csv file.

    Reads the specified csv file and returns the selected columns as a list.

    Args:
        csv_file:   A csv file instance.
        column_number:  The number of selected columns.
        split_num:  Number of segments of the list.

    Returns:
        column: A list includes the selected column values.

    Raises:
        NameError:  Error occurred when entering a csv file name without double quotes on both sides.
    """

    #   File Handling
    with open(csv_file) as csvfile:
        reader = csv.reader(csvfile)
        #   Retrieves the selected column from the csv file.
        column = [row[column_number] for row in reader]

        #   Split the read csv file into the desired fraction.
        column = np.array_split(column, split_num)

    return column


""" test function   """
def main():
    #   test "splitting_csv" function.
    tmp = splitting_csv("AComp_Passenger_data_no_error.csv", 1, 20)

    #   check type of "splitting_csv" return value.
    print(type(tmp))

    #   check the value in list.
    print(tmp[1])
    


if __name__ == '__main__':
    main()
