import pandas as pd

def task2_wirte2csv(output_high_to_low):
    """ Collate the data after the reduce.

    The entire data is traversed and collated and finally written to the \\output_data\\task1_output_check.csv file.

    Args:
        output_high_to_low:   data after the reduce.

    """
    #   store Passengers IDs    
    passengers = []
    #   store passengers flight times
    flight_times = []

    for passenger, times in output_high_to_low:
        #print(passenger,times)
        passengers.append(passenger)
        flight_times.append(times)

    #print(passengers)
    #print(flight_times)


    #   Create a Dataframe to store these data.
    df = pd.DataFrame(list(zip(passengers, flight_times)), columns=['passenger_id', 'flight_times'])

    #   Write to csv file.
    df.to_csv(r'output_data\task2_output_check.csv', index=False)

if __name__ == '__main__':
    output_high_to_low = [('UES9151GS5', 25), ('PUD8209OG3', 23), ('BWI0520BG6', 23), ('DAZ3029XA0', 23), ('SPR4484HA6', 23), ('EZC9678QI6', 21), ('HCA3158QA6', 21), ('JJM4724RF7', 21), ('POP2875LH3', 19), ('WYU2010YH8', 19), ('CKZ3132BR4', 19), ('WBE6935NU3', 19), ('HGO4350KK1', 18), ('CXN7304ER2', 17), ('YMH6360YP0', 16), ('JBE2302VO4', 16), ('SJD8775RZ4', 16), ('VZY2993ME1', 16), ('LLZ3798PE3', 16), ('WTC9125IE5', 14), ('MXU9187YC7', 14), ('EDV2089LK5', 13), ('XFG5747ZT9', 13), ('ONL0812DH1', 12), ('CDC0302NN5', 12), ('CYJ0225CH1', 11), ('KKP5277HZ7', 11), ('PAJ3974RK1', 10), ('IEG9308EA5', 10), ('PIT2755XC1', 8), ('UMH6360YP0', 1)]
    task2_wirte2csv(output_high_to_low)