def task1_check(input_value):
    """ Check format.

    Used to check that the input list has the correct type of element and that it meets the specified requirements.

    Args:
        input_value:   A list include value.

    Returns:
        tmp: A list with the non-formatted items removed, with all the elements of the original input list.
    """

    #   Used to store the index of the element to be deleted
    remove_value = []
    count = 0

    #   iterate list
    for i in input_value:

        #   If it is empty or contains spaces.
        if i.isspace() == True or len(i)==0 :
            print(f'The name {i} is incorrectly formatted, with spaces or empty values in the {count} position.')
            remove_value.append(count)
            
        #   If all letters are not all upper case or contain elements where all letters are not letters or numbers.
        elif    i.isupper() == False or i.isalnum() == False:
            print(f'The name {i} is incorrectly formatted, contains incorrect symbols or is incorrectly case-sensitive and is in the {count} position.')
            remove_value.append(count)
        count = count + 1

    #print(remove_value)

    #   Delete the elements of the input list according to the index in "remove-value".
    tmp = [input_value[i] for i in range(len(input_value)) if (i not in remove_value)]
    return tmp


#   Test Function
if __name__ == '__main__':
    test = ['DEN','JFK', 'ORD' ,'ORD' ,'DEN', 'KUL', 'MAD' ,'D%N', '', ' ', 'LHR', 'JFK', 'CGK']
    print(task1_check(test))