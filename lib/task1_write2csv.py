import pandas as pd

def task1_wirte2csv(output_high_to_low):
    """ Collate the data after the reduce.

    The entire data is traversed and collated and finally written to the \\output_data\\task1_output_check.csv file.

    Args:
        output_high_to_low:   data after the reduce.

    """

    #   Reads the csv file and corresponds the abbreviation to the name and extracts the corresponding value.
    df = pd.read_csv(r"input_data\Top30_airports_LatLong.csv", header=None)

    #   Name of the city where the airport is stored.
    city_names = []
    #   Storage airport abbreviation.
    city_codes = []
    #   Number of flights stored at the airport.
    flight_times = []

    for code, times in output_high_to_low:
        #   In the task1 check, there is no corresponding row in the Top30_airports_LatLong.csv file that is saved, and the error is due to the return of an empty dataframe.
        try:
            city_names.append(df.loc[df[1]==code][0].values[0])
            city_codes.append(code)
            flight_times.append(times)
        
        #   Used to deal with the problem of abbreviations for airports that do not exist in the file being read.
        except IndexError:
            print(f'the code {code} does not exist in the Top30_airports_LatLong.csv and is ignored.')
            pass


  

    #print(city_names)
    #print(city_codes)
    #print(flight_times)

    #   Create a Dataframe to store these data.
    df1 = pd.DataFrame(list(zip(city_names, city_codes, flight_times)), columns=['city_names', 'city_codes', 'flight_times'])

    #   Write to csv file.
    df1.to_csv(r'output_data\task1_output_check.csv', index=False)

if __name__ == '__main__':
    output_high_to_low = [('DEN', 46), ('IAH', 37), ('CAN', 36), ('ATL', 36), ('KUL', 33), ('ORD', 32), ('CGK', 26), ('JFK', 25), ('LHR', 25), ('CDG', 21), ('CLT', 21), ('PVG', 20), ('LAS', 17), ('BKK', 17), ('FCO', 15), ('MUC', 14), ('MAD', 13), ('AMS', 13), ('HND', 13), ('PEK', 12), ('DFW', 11), ('MIA', 11), ('UGK', 1), ('OR8', 1)]
    task1_wirte2csv(output_high_to_low)