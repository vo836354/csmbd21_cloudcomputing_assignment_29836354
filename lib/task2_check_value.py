def task2_check(input_value):
    """ Check format.

    Used to check that the input list has the correct type of element and that it meets the specified requirements.

    Args:
        input_value:   A list include value.

    Returns:
        tmp: A list with the non-formatted items removed, with all the elements of the original input list.
    """

    #   Used to store the index of the element to be deleted
    remove_value = []
    count = 0

    #   iterate list
    for i in input_value:

        #   If it is empty or contains spaces.
        if i.isspace() == True or len(i)==0 :
            print(f'The passenger ID {i} is incorrectly formatted, with spaces or empty values in the {count} position.')
            remove_value.append(count)

        #   Determine if bits 1-3 are uppercase, bits 3-7 are numeric, bits 7-9 are uppercase, and bits 9-10 are numeric.  
        elif    not(i[0:3].isupper()) or not(i[3:7].isdigit()) or not(i[7:9].isupper()) or not(i[9:10].isdigit()):
            print(f'There is a problem with the passenger ID {i} format and it is planned to delete this data.')
            remove_value.append(count)

        count = count + 1

    #print(remove_value)
    tmp = [input_value[i] for i in range(len(input_value)) if (i not in remove_value)]
    return tmp

if __name__ == '__main__':
    test = ['UES9151GS5', '', 'UES9151GS5', 'sEs915*GS5', 'EDV2089LK5']
    print(task2_check(test))