#   map.py
import sys

def map_func(x):
    """ Map Function

    Each map process takes each word as a key, takes 1 as the word frequency number value, and outputs it.

    Args:
        x:   key.

    Returns:
        (x,1): key/value pairs.
    """
    return (x,1)


if __name__ == '__main__':

    map_in = ['to', 'be', 'or', 'not', 'to', 'be']
    map_out = map(map_func,map_in)
    print(list(map_out))
