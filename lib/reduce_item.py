# reduce_item.py

def reduce_func(item):
    """ Reduce Function

    After receiving the shuffle data, the same words are merged to get the word frequency of each word, and finally the counted word frequency of each word is used as the output result.

    Args:
        item:   Shuffle data.

    Returns:
        (word, sum(occurances)): The number of word frequencies counted for each word.
    """
    word, occurances = item
    return (word, sum(occurances))