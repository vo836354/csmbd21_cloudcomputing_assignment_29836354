#   shuffle.py
import sys

def shuffle(mapper_out):
    """ Shuffling Function

    The output of each map is shuffled, placing the same word key inside a bucket, which is then handed over to reduce for processing.

    Args:
        mapper_out:   Output of map.

    Returns:
        data: Data after Shuffling..
    """

    #   store data after shuffling.
    data={}
    for k,v in mapper_out:
        if k not in data:
            data[k] = [v]
        else:
            data[k].append(v)
    return  data


if __name__ == '__main__':
    mapper_out=[("a",1), ("a",1), ("ab",1), ("ab",1), ("a",1), ("ac",1),]
    print(shuffle(mapper_out))